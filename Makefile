#!/usr/bin/make -f

PDFLATEX	?= pdflatex -halt-on-error -file-line-error
BIBTEX		?= bibtex
PDFVIEWER	?= xdg-open

BASENAME 	= research-summary
TEXT_OUT	= $(BASENAME).txt
PDFTARGET	= $(BASENAME).pdf
SOURCE 		= $(BASENAME).tex
HTML_OUT_DIR	= html
HTML_OPTS 	= -dir $(HTML_OUT_DIR) -split 0 -no_navigation -info 0 -address 0


text: html
	html2text -width 70 $(HTML_OUT_DIR)/index.html > $(TEXT_OUT)

html: create_dir pdf
	latex2html $(HTML_OPTS) $(SOURCE)
	sed -i 's/^Bibliography/Publications/g' $(HTML_OUT_DIR)/*.html

create_dir:
	mkdir -p $(HTML_OUT_DIR)

pdf:
	$(PDFLATEX) $(BASENAME)
	$(BIBTEX)   $(BASENAME)
	$(PDFLATEX) $(BASENAME)
	$(PDFLATEX) $(BASENAME)

all: text

view: pdf
	$(PDFVIEWER) $(PDFTARGET)

clean:
	$(RM) -rf $(HTML_OUT_DIR)
	$(RM) $(TEXT_OUT)
	$(foreach EXT,aux bbl blg log pdf out,$(RM) $(BASENAME).$(EXT);)
